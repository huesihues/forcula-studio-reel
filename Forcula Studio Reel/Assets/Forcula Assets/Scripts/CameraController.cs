﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CameraController : MonoBehaviour
{
    class CameraState
    {
        public float yaw;
        public float pitch;
        public float roll;


        public void SetFromTransform(Transform t)
        {
            pitch = t.eulerAngles.x;
            yaw = t.eulerAngles.y;
            roll = t.eulerAngles.z;
        }

        public void Translate(Vector3 translation)
        {
            Vector3 rotatedTranslation = Quaternion.Euler(pitch, yaw, roll) * translation;

        }

        public void LerpTowards(CameraState target, float rotationLerpPct)
        {
            yaw = Mathf.Lerp(yaw, target.yaw, rotationLerpPct);
            pitch = Mathf.Lerp(pitch, target.pitch, rotationLerpPct);
            roll = Mathf.Lerp(roll, target.roll, rotationLerpPct);
        }

        public void UpdateTransform(Transform t)
        {
            t.eulerAngles = new Vector3(pitch, yaw, roll);
        }
    }

    CameraState m_TargetCameraState = new CameraState();
    CameraState m_InterpolatingCameraState = new CameraState();
    public GameManager gameManager;
    public GameObject reticle;
    public Image reticleFull;
    public Transform objectSelected;
    public Transform objectPosition;

    [Header("Info Settings")]
    public bool rotating;
    public bool impactado;
    public bool interactuable = true;
    public bool lerping;
    public float speed;
    public float position;


    [Tooltip("X = Change in mouse position.\nY = Multiplicative factor for camera rotation.")]
    public AnimationCurve sensitivityCurveSpeed = new AnimationCurve(new Keyframe(0f, 0.5f, 0f, 5f), new Keyframe(1f, 2.5f, 0f, 0f));

    [Header("Rotation Settings")]
    [Tooltip("X = Change in mouse position.\nY = Multiplicative factor for camera rotation.")]
    public AnimationCurve mouseSensitivityCurve = new AnimationCurve(new Keyframe(0f, 0.5f, 0f, 5f), new Keyframe(1f, 2.5f, 0f, 0f));

    [Tooltip("Time it takes to interpolate camera rotation 99% of the way to the target."), Range(0.001f, 1f)]
    public float rotationLerpTime = 0.01f;

    [Tooltip("Whether or not to invert our Y axis for mouse input to rotation.")]
    public bool invertY = false;

    public float time;

    // Time when the movement started.
    public float startTime;
    public Vector3 startPosition;

    void OnEnable()
    {
        m_TargetCameraState.SetFromTransform(transform);
        m_InterpolatingCameraState.SetFromTransform(transform);
    }

    // Update is called once per frame
    void Update()
    {
        if (interactuable)
        {
            UpdateRotation();
            PointView();
            ReticleManager();
        }
        
        CameraSettings();

        if (rotating)
        {
            RayCaster();
            
        }
        if (impactado && interactuable && rotating)
        {
            Countdown();
        }
        else
        {
            time = 0;
        }
        if (lerping)
        {
            TranslateToPosition();

        }
    }
    void CameraSettings()
    {
        // Exit Sample  
        if (Input.GetKey(KeyCode.Escape))
        {
            Application.Quit();
#if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
#endif
        }
        Debug.Log(Input.GetMouseButtonDown(1));

        // Hide and lock cursor when right mouse button pressed
        if (Input.GetMouseButtonDown(1))
        {
            Cursor.lockState = CursorLockMode.Locked;
            rotating = true;
        }

        // Unlock and show cursor when right mouse button released
        if (Input.GetMouseButtonUp(1))
        {
            rotating = false;
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;
        }
    }
    void UpdateRotation()
    {
        // Rotation
        if (Input.GetMouseButton(1))
        {
            var mouseMovement = new Vector2(Input.GetAxis("Mouse X"), Input.GetAxis("Mouse Y") * (invertY ? 1 : -1));

            var mouseSensitivityFactor = mouseSensitivityCurve.Evaluate(mouseMovement.magnitude);

            m_TargetCameraState.yaw += mouseMovement.x * mouseSensitivityFactor;
            m_TargetCameraState.pitch += mouseMovement.y * mouseSensitivityFactor;
        }



        // Framerate-independent interpolation
        // Calculate the lerp amount, such that we get 99% of the way to our target in the specified time

        var rotationLerpPct = 1f - Mathf.Exp((Mathf.Log(1f - 0.99f) / rotationLerpTime) * Time.deltaTime);
        m_InterpolatingCameraState.LerpTowards(m_TargetCameraState, rotationLerpPct);

        m_InterpolatingCameraState.UpdateTransform(transform);
    }
    void RayCaster()
    {
        Ray ray = transform.GetChild(0).GetComponent<Camera>().ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        if(Physics.Raycast(ray,out hit))
        {
            objectSelected = hit.transform;
            if (objectSelected.CompareTag("Interactuable"))
            {
                impactado = true;
            }
            else
            {
                impactado = false;
            }
        }
    }

    void PointView()
    {
        reticle.SetActive(rotating);
    }

    void Countdown()
    {
        if(time < gameManager.timeCountdown)
        {
            time += Time.deltaTime;
        }
        else
        {
            interactuable = false;
            rotating = false;
            time = 0;
            impactado = false;
            StartTranslateToPosition();
            SetTargetDown();
        }
    }

    void StartTranslateToPosition()
    {
        startPosition = transform.position;
        lerping = true;
    }
    void TranslateToPosition()
    {
        startTime += Time.deltaTime;
        float senibility =sensitivityCurveSpeed.Evaluate(position);

        position = gameManager.cameraSpeed * startTime * senibility * speed;
        // Set our position as a fraction of the distance between the markers.
        transform.position = Vector3.Lerp(startPosition, objectSelected.position,position );

        
        if (position > 0.98f)
        {
            position = 0;
            startTime = 0;
            lerping = false;
            interactuable = true;
            if (Input.GetMouseButton(1))
            {
                rotating = true;
            }
        }
    }
    void SetTargetDown()
    {
        if (objectPosition != null)
        {
            objectPosition.parent.GetComponent<Renderer>().enabled = true;
            objectPosition.gameObject.SetActive(true);
        }
        objectSelected.parent.GetComponent<Renderer>().enabled = false;
        objectSelected.gameObject.SetActive(false);

        objectPosition = objectSelected;
    }

    void ReticleManager()
    {
        reticleFull.fillAmount = time / gameManager.timeCountdown;
    }
}


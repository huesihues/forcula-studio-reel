﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IRotador 
{
    float speedRotation {get; }
    void Rotacion();
}

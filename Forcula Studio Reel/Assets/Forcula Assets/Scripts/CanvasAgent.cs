﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CanvasAgent : MonoBehaviour
{
    public CanvasGroup canvasIntro;
    public CameraController cameraController;
    public float speedFadeOut = 1;
    public bool infoEnPantalla = true;
    public bool moviendo = false;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (infoEnPantalla)
        {
            if (cameraController.rotating)
            {
                moviendo = true;
            }
            if (moviendo)
            {
                FadeOutCanvasIntro();
            }
        }
    }
    void FadeOutCanvasIntro()
    {
        canvasIntro.alpha -= canvasIntro.alpha * Time.deltaTime * speedFadeOut;
        if (canvasIntro.alpha <= 0)
        {
            infoEnPantalla = false;
        }
    }
}

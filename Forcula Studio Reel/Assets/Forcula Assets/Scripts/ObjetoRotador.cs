﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjetoRotador : MonoBehaviour,IRotador
{

    public float speed = 2.0f;
    public float speedRotation { get => speed; }

    public void Rotacion()
    {
        transform.Rotate(new Vector3(0, speedRotation * Time.deltaTime, 0));
    }


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Rotacion();
    }
}
